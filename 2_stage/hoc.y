%{
#include <stdio.h>
#include <signal.h>
#include <setjmp.h>

void execerror(char const *s, char const *t);

double mem[26];       /* memory for variables 'a'..'z' */
%}
%union {             /* stack type */
        double val;  /* actual value */
        int index;   /* index into mem[] */
}
%token <val>   NUMBER
%token <index> VAR
%type  <val>   expr
%right '='
%left  '+' '-' /* left associative, same precedence */
%left  '*' '/' /* left associative, higher precedence */
%left UNARYMINUS
%%
list:    /* nothing */
       | list '\n'
       | list expr '\n'   { printf("\t%.8g\n", $2); }
       | list error '\n' { yyerrok; }
       ;

expr:    NUMBER
       | VAR           { $$ = mem[$1]; }
       | VAR '=' expr  { $$ = mem[$1] = $3; }
       | expr '+' expr { $$ = $1 + $3; }
       | expr '-' expr { $$ = $1 - $3; }
       | expr '*' expr { $$ = $1 * $3; }
       | expr '/' expr { if ($3 == 0.0)
	                   execerror("division by zero", "");
                         $$ = $1 / $3; }
       | '(' expr ')'  { $$ = $2; }
       | '-' expr %prec UNARYMINUS { $$ = -$2; }
       ;
%%
       /* end of grammar */

char *progname;
int  lineno = 1;
jmp_buf begin;

// handler for floating point exception
void fpecatch(int sig);

int main(int argc, char *argv[])
{
  progname = argv[0];
  setjmp(begin);
  /* 
     In the original source code, Kernighan and Pike added the SIGFPE handler to deal with floating point overflow
     and to avoid that the program stops due to the corresponding run-time error.
     This is not a problem in the current version of C anymore because expressions such as 1e600 and 0.0 / 0.0 are treated as inf
     and the program does not abort automatically. 

     There seems to be a problem with the current implementation.  SIGFPE is never raised for the above two expressions. Why???  
     However I can raise it artficially by invoking raise(SIGFPE).
   */
  signal(SIGFPE, fpecatch);
  yyparse();
  return 0;
}

int yylex(void) 
{
  int c;

  while ((c=getchar()) == ' ' || c == '\t')
    ;
  if (c == EOF)
    return 0;
  if (c == '.' || isdigit(c)) {
    ungetc(c, stdin);
    scanf("%lf", &yylval.val);
    return NUMBER;
  }
  if (islower(c)) {
    yylval.index = c - 'a'; /* ASCII only */
    return VAR;
  }
  if (c == '\n')
    lineno++;
  return c;
}

void warning(char const *s, char const *t)
{
  fprintf(stderr, "%s: %s", progname, s);
  if (t && (strcmp(t, "") != 0))
    fprintf(stderr, " %s", t);
  fprintf(stderr, " near line %d\n", lineno);
}

void execerror(char const *s, char const *t) /* recover from run-time error */
{
  warning(s, t);
  longjmp(begin, 0);
} 

void fpecatch(int sig) /* catch floating point exceptions */
{
  execerror("floating point exception", (char *) 0);
}

int yyerror(char const *s)
{
  warning(s, (char *) 0);
  return 0;
}




