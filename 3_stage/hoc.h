/*
  hoc.h
  Global data structures for inclusion
*/

typedef struct Symbol { /* symbol table entry */
  char  *name;
  short type;            /* VAR, BLTIN, UNDEF */
  union {
    double val;            /* if VAR */
    double (*ptr)();       /* if BLTIN */
  } u;
  struct Symbol *next;   /* link to another */
} Symbol;

Symbol *install(), *lookup();
