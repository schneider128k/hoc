/*
  symbol.c
  Symbol table routines: lookup, install
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hoc.h"
#include "y.tab.h"

static Symbol *symlist = 0; /* symbol table: linked list */

Symbol *lookup(char *s)     /* find s in symbol table */ 
{
  Symbol *sp;

  for (sp = symlist; sp != (Symbol *) 0; sp = sp-> next)
    if (strcmp(sp->name, s) == 0)
      return sp;
  return 0;
}

Symbol *install(char *s, int t, double d) /* install s in symbol table */
{
  Symbol *sp;
  char *emalloc();

  sp = (Symbol *) emalloc(sizeof(Symbol));
  sp->name = emalloc(strlen(s)+1);         /* +1 for '\n' */
  strcpy(sp->name, s);
  sp->type = t;
  sp->u.val = d;
  sp->next = symlist; /* put at front of list */
  symlist = sp;
  return sp;
}

char *emalloc(unsigned n) /* check return from malloc */
{
  char *p;

  p = malloc(n);
  if (p == 0)
    printf("out of memory\n");
  /* replace by execerror */
  return p;
}

/* PW: added this function for debugging purposes */
void printsymboltable()    
{
  Symbol *sp;

  for (sp = symlist; sp != (Symbol *) 0; sp = sp-> next) {
    printf("name: %s\n", sp->name);
  }
}

/* PW: added main function for testing purposes */
/* 
int main() {
  install("a", 1, 1.0);
  install("b", 1, 2.0);
  install("c", 1, 3.0);
  install("d", 1, 4.0);
  printf("the contents of the symbol table are:\n");
  printsymboltable();
  printf("\nlooking up c\n");
  Symbol *sp;
  
  sp = lookup("c");
  printf("name: %s\n", sp->name);
  printf("name: %lf\n", sp->u.val);
}

*/
