%{
#include "hoc.h"
#include <stdio.h>
#include <math.h>
#include <signal.h>
#include <setjmp.h>

void execerror(char const *s, char const *t);

%}
%union {            
        double val;  /* actual value */
        Symbol *sym; /* index into mem[] */
}
%token <val>   NUMBER
%token <sym>   VAR BLTIN UNDEF
%type  <val>   expr asgn
%right '='
%left  '+' '-' /* left associative, same precedence */
%left  '*' '/' /* left associative, higher precedence */
%left  UNARYMINUS
%right '^'
%%
list:    /* nothing */
       | list '\n'
       | list asgn '\n'
       | list expr '\n'   { printf("\t%.8g\n", $2); }
       | list error '\n' { yyerrok; }
       ;

asgn:    VAR '=' expr { $$=$1->u.val=$3; $1->type = VAR; }
       ;

expr:    NUMBER
       | VAR { if ($1->type == UNDEF)
                 execerror("undefined variable", $1->name);
               $$ = $1->u.val; }
       | asgn
       | BLTIN '(' expr ')' { $$ = (*($1->u.ptr))($3); }
       | expr '+' expr { $$ = $1 + $3; }
       | expr '-' expr { $$ = $1 - $3; }
       | expr '*' expr { $$ = $1 * $3; }
       | expr '/' expr { if ($3 == 0.0)
	                   execerror("division by zero", "");
                         $$ = $1 / $3; }
       | expr '^' expr { $$ = pow($1, $3); }
       | '(' expr ')'  { $$ = $2; }
       | '-' expr %prec UNARYMINUS { $$ = -$2; }
       ;
%%
       /* end of grammar */

char *progname;
int  lineno = 1;
jmp_buf begin;

// handler for floating point exception
void fpecatch(int sig);

int main(int argc, char *argv[])
{
  progname = argv[0];
  init();
  setjmp(begin);
  /* 
     In the original source code, Kernighan and Pike added the SIGFPE handler to deal with floating point overflow
     and to avoid that the program stops due to the corresponding run-time error.
     This is not a problem in the current version of C anymore because expressions such as 1e600 and 0.0 / 0.0 are treated as inf
     and the program does not abort automatically. 

     There seems to be a problem with the current implementation.  SIGFPE is never raised for the above two expressions. Why???  
     However I can raise it artficially by invoking raise(SIGFPE).
   */
  signal(SIGFPE, fpecatch);
  yyparse();
  return 0;
}

int yylex(void) 
{
  int c;

  while ((c=getchar()) == ' ' || c == '\t')
    ;
  if (c == EOF)
    return 0;
  if (c == '.' || isdigit(c)) {
    ungetc(c, stdin);
    scanf("%lf", &yylval.val);
    return NUMBER;
  }
  if (isalpha(c)) {
    Symbol *s;
    char sbuf[100], *p = sbuf;
    do {
      *p++ = c;
    } while ((c=getchar()) != EOF && isalnum(c));
    ungetc(c, stdin);
    *p = '\0';
    if ((s=lookup(sbuf)) == 0)
      s = install(sbuf, UNDEF, 0.0);
    yylval.sym = s;
    return s->type == UNDEF ? VAR : s->type;
  }
  if (c == '\n')
    lineno++;
  return c;
}

void warning(char const *s, char const *t)
{
  fprintf(stderr, "%s: %s", progname, s);
  if (t && (strcmp(t, "") != 0))
    fprintf(stderr, " %s", t);
  fprintf(stderr, " near line %d\n", lineno);
}

void execerror(char const *s, char const *t) /* recover from run-time error */
{
  warning(s, t);
  longjmp(begin, 0);
} 

void fpecatch(int sig) /* catch floating point exceptions */
{
  execerror("floating point exception", (char *) 0);
}

int yyerror(char const *s)
{
  warning(s, (char *) 0);
  return 0;
}




