/*
  init.c
  built-ins and constants: init
*/

#include "hoc.h"
#include "y.tab.h"
#include <math.h>

//extern double Log(), Log10(), Exp(), Sqrt(), integer();

extern double integer(double);

static struct { /* constants */
  char *name;
  double cval;
} consts[] = {
  "PI",    3.14159,
  "E",     2.71828,
  "GAMMA", 0.57721,
  "DEG",   57.295779,
  "PHI",   1.61803,
  0, 0
};

static struct { /* built-ins */
  char *name;
  double (*func)(double);
} builtins[] = {
  "sin",   sin,
  "cos",   cos,
  "atan",  atan,
  "log",   log, //Log,
  "log10", log10, //Log10,
  "exp",   exp, // Exp,
  "sqrt",  sqrt, //Sqrt,
  "int",   integer,
  "abs",   fabs,
  0,       0
};

void init() {
  int i;
  Symbol *s;

  for (i = 0;  consts[i].name; i++)
    install(consts[i].name, VAR, consts[i].cval);

  for (i = 0;  builtins[i].name; i++) {
    s = install(builtins[i].name, BLTIN, 0.0);
    s->u.ptr = builtins[i].func;
  }
}

