%{
#define YYSTYPE double /* data type of yacc stack */
#include <stdio.h>
%}
%token NUMBER
%left  '+' '-' /* left associative, same precedence */
%left  '*' '/' /* left associative, higher precedence */
%left UNARYMINUS
%%
list:    /* nothing */
       | list '\n'
       | list expr '\n'   { printf("\t%.8g\n", $2); }
       ;

expr:    NUMBER        { $$ = $1; }
       | '-' expr %prec UNARYMINUS { $$ = -$2; }
       | expr '+' expr { $$ = $1 + $3; }
       | expr '-' expr { $$ = $1 - $3; }
       | expr '*' expr { $$ = $1 * $3; }
       | expr '/' expr { $$ = $1 / $3; }
       | '(' expr ')'  { $$ = $2; }
       ;
%%
       /* end of grammar */

char *progname;
int  lineno = 1;

int yyerror(char const *s);

void warning(char const *s, char const *t);

int main(int argc, char *argv[])
{
  progname = argv[0];
  yyparse();
  return 0;
}

int yylex() 
{
  int c;

  while ((c=getchar()) == ' ' || c == '\t')
    ;
  if (c == EOF)
    return 0;
  if (c == '.' || isdigit(c)) {
    ungetc(c, stdin);
    scanf("%lf", &yylval);
    return NUMBER;
  }
  if (c == '\n')
    lineno++;
  return c;
}

int yyerror(char const *s)
/* 
http://www.gnu.org/software/bison/manual/html_node/Error-Reporting.html

Traditionally yyerror returns an int that is always ignored, but this is purely for historical reasons, 
and void is preferable since it more accurately describes the return type for yyerror. 

I decided to keep int as return type so the complier does not produce a warning.
*/
{
  warning(s, (char *) 0);
  return 0;
}

void warning(char const *s, char const *t)
{
  fprintf(stderr, "%s: %s", progname, s);
  if (t)
    fprintf(stderr, " %s", t);
  fprintf(stderr, " near line %d\n", lineno);
}
