/*
  hoc.h
  Global data structures for inclusion
*/

typedef struct Symbol { /* symbol table entry */
  char  *name;
  short type;            /* VAR, BLTIN, UNDEF */
  union {
    double val;            /* if VAR */
    double (*ptr)();       /* if BLTIN */
  } u;
  struct Symbol *next;   /* link to another */
} Symbol;

Symbol *install(), *lookup();

typedef union Datum { /* interpreter stack type */
  double val;
  Symbol *sym;
} Datum;

extern Datum pop();

typedef void (*Inst)(); /* machine instruction */

#define STOP (Inst) 0

extern Inst prog[];
extern void eval(), add(), sub(), mul(), division(), negate(), power();
extern void assign(), bltin(), varpush(), constpush(), print();
