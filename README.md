Higher order calculator from the book "The Unix Programming Environment" by Brian W. Kernighan and Rob Pike, Prentice-Hall Software Series, 1984. 

You can see the original hoc code in chapter 8 of the above book.  The pdf file is available here: 
http://www.eecs.ucf.edu/~wocjan/Teaching/2015_Summer/COP3402/documents/lectures/upe.pdf

I had to make several changes because of the changes in C and yacc/bison over the last 30 years.  

I changed all the function declarations.  See the book "The C Programming Language" (Second edition) by Brian W. Kernighan and Dennis M. Ritchie on page 26 for a comparison of the old and new styles of function declaration.

You can read about yacc/bison at http://www.gnu.org/software/bison/manual/bison.html
